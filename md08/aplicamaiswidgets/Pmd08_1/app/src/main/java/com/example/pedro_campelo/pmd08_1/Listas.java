package com.example.pedro_campelo.pmd08_1;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


/**
 * Created by pedro_campelo on 04/08/2016.
 */
public class Listas extends ListActivity {

    //sendo uma váriavel global,a variável estados pode ser utilizada em outros métodos dentro da classe
    public String[]estados;


    public void onCreate(Bundle bundle){
        super.onCreate(bundle);

        //Cria um array que recebe os valores utilizados no ListView
        estados=new String[]{"São Paulo","Rio de Janeiro","Minas Gerais","Rio Grande do Sul","Santa Catarina","Paraná","Mato Grosso","Amazonas"};

    //Cria um ArrayAdapter para que a lista criada acima possa aparecer no componente ListView
        this.setListAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,estados));
    }

    //Evento que identifica o item selecionado na lista
    public void onListItemClick(ListView l,View v,int position,long id){
        super.onListItemClick(l,v,position,id);

        //Identifica o item clicado pela posição do mesmo
        Object o=this.getListAdapter().getItem(position);

        String listaEstados=o.toString();

        //Exibe uma mensagem apresentando na tela o item clicado
        /*Os passametros utilizados com Toast são:a classe em questão que vai utilizar o Toast,neste caso como aponta para a mesma activity, é utilizado o comando this.
        Além disso é passado um parametro contendo um valor de qualquer tipo, e logo após finalizando com o comando que define o tempo em tela da mensagem.*/
        Toast.makeText(this,"Você selecionou:"+listaEstados,Toast.LENGTH_SHORT).show();
    }

}
