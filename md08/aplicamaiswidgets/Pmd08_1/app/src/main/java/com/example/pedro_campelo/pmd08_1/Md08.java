package com.example.pedro_campelo.pmd08_1;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class Md08 extends Activity  implements OnClickListener {
    Spinner listaCursos;

    //Cria um array que recebe os valores utilizados no componente Spinner
    static final String[] cursos=new String[]
            {"Java Fundamentos","Android Básico","Android Avançado"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_md08);

    //Cria um ArrayAdapter para que a lista criada acima possa aparecer no componente Spinner
    ArrayAdapter<String> add= new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,cursos);


    //Cria o componente Spinner selecionando o id utilizado no layout
    listaCursos=(Spinner)findViewById(R.id.spinnerCursos);

    //Adiciona o ArrayAdapter no array
    listaCursos.setAdapter(add);

        Button ok=(Button)findViewById(R.id.btnOK);

        //Evento que será acionado quando o botão for clicado
        ok.setOnClickListener(this);
    }


    /*public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.telaprincipal,menu);
        return true;
    }*/

    @Override
    public void onClick(View v) {


        TextView status=(TextView)findViewById(R.id.txtStatus);
        int op=listaCursos.getSelectedItemPosition();

        /*O componente TextView status recebe o texto indicando o nome do curso, que é localizado utilizando sua posição*/
        status.setText("Status: Curso "+cursos[op]);
    }
}
