package com.example.pedro_campelo.pmd08_1;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

/**
 * Created by pedro_campelo on 04/08/2016.
 */
public class Checkbox extends Activity{

    private CheckBox iOS, android, WPhone;
    private Button botao;

    public void onCreate(Bundle bundle){

        //O parametro bundle é passado para o método sobrescrito para que a view da activity possa ser criada
        super.onCreate(bundle);
        setContentView(R.layout.checkbox);

        //Estas ações serão adicionadas ao aplicativo assim que a atividade for iniciada
        addListenerOnChkIos();
        addListenerOnButton();
    }


    //Este método irá mostrar uma mensagem ao usuário caso a opção iPhone for selecionada
    public void addListenerOnChkIos(){

            iOS=(CheckBox)findViewById(R.id.checkApple);

    iOS.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if(((CheckBox)v).isChecked()){
                Toast.makeText(Checkbox.this, "Cara,tente utilizar o Android!!!!!", Toast.LENGTH_SHORT).show();
            }

        }
    });

    }

    //Este método identifica as opções selecionadas e mostra elas a partir do momento em que o botão for clicado
    public void addListenerOnButton(){
        iOS=(CheckBox)findViewById(R.id.checkApple);
        android=(CheckBox)findViewById(R.id.checkAndroid);
        WPhone=(CheckBox)findViewById(R.id.checkWindows);

        botao=(Button)findViewById(R.id.btnEnviar);


        //Evento que será utilizado para mostrar as mensagens para o usuário depois de clicar no botão
        botao.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                //Esta classe é utilizada para receber uma ou várias mensagens de alertas para mostrar ao usuário conforme opção selecionada
                StringBuffer buffer = new StringBuffer();
                buffer.append("Opção IPhone:").append(iOS.isChecked());
                buffer.append("\nOpção Android:").append(android.isChecked());
                buffer.append("\nOpção Windows Phone:").append(WPhone.isChecked());

                Toast.makeText(Checkbox.this,buffer.toString(),Toast.LENGTH_SHORT).show();
            }
        });

    }

}
