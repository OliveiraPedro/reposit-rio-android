package com.example.pedro_campelo.pmd08_1;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.view.View.OnClickListener;
import android.widget.Toast;

/**
 * Created by pedro_campelo on 04/08/2016.
 */
public class RadioList extends Activity{

    private RadioGroup radioGrupoSexo;
    private RadioButton radioBotaoSexo;
    private Button botao;

    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.exemplo_radio_button);

        addListenerOnButton();
    }


    /*Este método identifica qual das opções dentro do radiogroup foi selecionada para mostrar mensagem assim que o evento ligado ao botão for acionado*/
    public void addListenerOnButton(){

        radioGrupoSexo=(RadioGroup)findViewById(R.id.radioSex);
        botao=(Button)findViewById(R.id.btnEnviar);

        botao.setOnClickListener(new OnClickListener(){

            @Override
            public void onClick(View v) {

                //Recupera o id do radio button selecionado dentro do grupo
                int idGrupoRadio=radioGrupoSexo.getCheckedRadioButtonId();

                //Encontra o radiobutton selecionado pelo id recuperado anteriormente
                radioBotaoSexo=(RadioButton)findViewById(idGrupoRadio);

                //Dentro dos parametros convencionais, a função makeText recebe o texto do radio button selecionado
                Toast.makeText(RadioList.this,radioBotaoSexo.getText(),Toast.LENGTH_SHORT).show();
            }
        });


    }
}
