package com.example.pedro_campelo.pmd08_2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

public class Tela1 extends Activity implements View.OnClickListener{
    //String estados[];
    private RadioGroup radioGrupoSexo;
    private RadioButton radioBotaoSexo;
    private RadioGroup radioGrupoTipoTelefone;
    private RadioButton radioBotaoTelefone;


    private Button botao;
    private Spinner listaEstadoCivil;

    //Cria um array que recebe os valores utilizados no componente Spinner,
    // mas neste caso ele recebe valores previamente criados no arquivo de recursos strings.xml
  //static final String[] estadoCivil=new String[]{this.getString(R.string.solteiro),this.getString(R.string.casado),this.getString(R.string.divorciado)};
    // static final private String[] estadoCivil=new String[]{"teste"};
    static String[] estadoCivil;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela1);



        radioGrupoTipoTelefone=(RadioGroup)findViewById(R.id.radioGroupFone);
        int idGrupoRadioFone=radioGrupoTipoTelefone.getCheckedRadioButtonId();
        radioBotaoTelefone=(RadioButton)findViewById(idGrupoRadioFone);

        estadoCivil=getResources().getStringArray(R.array.tipoEstadoCivil);
        //Cria um ArrayAdapter para que a lista criada acima possa aparecer no componente Spinner
        ArrayAdapter<String> adicionaAdapter= new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,estadoCivil);
        listaEstadoCivil=(Spinner)findViewById(R.id.spinnerCivil);
        listaEstadoCivil.setAdapter(adicionaAdapter);


        botao=(Button)findViewById(R.id.btnEnviar);
        botao.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        //Recupera o texto digitado no campo nome
        EditText texto=(EditText)findViewById(R.id.editNome);
        String nome=texto.getText().toString();

        //Recupera o id do radio button selecionado dentro do grupo
        radioGrupoSexo=(RadioGroup)findViewById(R.id.radioGroupSexo);
        //Encontra o radiobutton selecionado pelo id recuperado anteriormente
        int idGrupoRadioSexo=radioGrupoSexo.getCheckedRadioButtonId();
        radioBotaoSexo=(RadioButton)findViewById(idGrupoRadioSexo);

        //Recupera a posição do elemento selecionado dentro do Spinner
        //Recupera o texto do elemento selecionado dentro do Spinner, sendo que utiliza a posição recupera anteriormente
        String situacaoCivil=estadoCivil[listaEstadoCivil.getSelectedItemPosition()];

        //Recupera o id do radio button selecionado dentro do grupo
        radioGrupoTipoTelefone=(RadioGroup)findViewById(R.id.radioGroupFone);
        //Encontra o radiobutton selecionado pelo id recuperado anteriormente
        int idGrupoRadioFone=radioGrupoTipoTelefone.getCheckedRadioButtonId();
        radioBotaoTelefone=(RadioButton)findViewById(idGrupoRadioFone);

        EditText fone=(EditText)findViewById(R.id.editFone);
        String telefone=fone.getText().toString();







        if(nome.toString().trim().equals("")){
            Toast.makeText(this,"Preencha corretamente o nome!!!!!",Toast.LENGTH_SHORT).show();
        }else if(telefone.toString().trim().equals(""))
            Toast.makeText(this,"Preencha corretamente o telefone!!!!!",Toast.LENGTH_SHORT).show();

        else{


            Intent intencao=new Intent(this,Tela2.class);

            Bundle parametros=new Bundle();
            parametros.putString("nome", nome);
            parametros.putString("sexo",radioBotaoSexo.getText().toString());
            parametros.putString("estadoCivil", situacaoCivil);
            parametros.putString("telefone", telefone);
            parametros.putString("tipoTelefone",radioBotaoTelefone.getText().toString());
            intencao.putExtras(parametros);

            //Caso todos os campos sejam preenchidos corretamente, o método startActivity() será iniciado junto com os valores que passarão para a tela2
            startActivity(intencao);
        }

        /*if(radioBotaoSexo.isChecked()==true){
            //Dentro dos parametros convencionais, a função makeText recebe o texto do radio button sexo selecionado
            Toast.makeText(this,"deu certo",Toast.LENGTH_SHORT).show();
        }*/

        /*if(radioBotaoTelefone.isChecked()){
            //Dentro dos parametros convencionais, a função makeText recebe o texto do radio button sexo selecionado
            Toast.makeText(this,radioBotaoTelefone.getText(),Toast.LENGTH_SHORT).show();
        }*/


    }
}
