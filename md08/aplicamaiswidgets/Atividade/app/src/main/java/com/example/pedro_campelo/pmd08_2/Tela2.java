package com.example.pedro_campelo.pmd08_2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by pedro_campelo on 05/08/2016.
 */
public class Tela2 extends Activity implements View.OnClickListener{
    Intent intencao;
    Bundle parametros;
    Button botao;

    Spinner listaCursos;
    static String[] cursos;

    Spinner listaLocais;
    static String[] locais;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela2);

        intencao=getIntent();
        parametros=intencao.getExtras();
        /*String texto1=parametros.getString("nome");
        String texto2=parametros.getString("sexo");
        String texto3=parametros.getString("estadoCivil");
        String texto4=parametros.getString("telefone");
        String texto5=parametros.getString("tipoTelefone");*/

        //Este componente captura o valor inserido no campo nome
        TextView nome=(TextView)findViewById(R.id.textNome2);
        nome.setText("Nome:"+parametros.getString("nome"));

        //Este componente captura o valor da opção sexo
        TextView sexo=(TextView)findViewById(R.id.textSexo2);
        sexo.setText("Sexo:"+parametros.getString("sexo"));

        //Este componente captura o valor de uma das opções de estado civil
        TextView estadoCivil=(TextView)findViewById(R.id.textEstadoCivil2);
        estadoCivil.setText("Estado Civil:"+parametros.getString("estadoCivil"));

        //Este componente captura o valor inserido no campo telefone
        TextView telefone= (TextView) findViewById(R.id.textFone2);
        telefone.setText("Telefone:"+parametros.getString("telefone"));

        //Este componente captura o valor da opção tipo de telefone
        TextView tipoTelefone=(TextView)findViewById(R.id.textTipoTelefone2);
        tipoTelefone.setText("Tipo do Telefone:"+parametros.getString("tipoTelefone"));

        cursos=getResources().getStringArray(R.array.tipoDoCurso);
        //Cria um ArrayAdapter para que a lista criada acima possa aparecer no componente Spinner
        ArrayAdapter<String>adicionaAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,cursos);
        listaCursos=(Spinner)findViewById(R.id.spinnerCurso);
        listaCursos.setAdapter(adicionaAdapter);

        locais=getResources().getStringArray(R.array.localCurso);
        //Cria um ArrayAdapter para que a lista criada acima possa aparecer no componente Spinner
        ArrayAdapter<String>adicionaAdapter2=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,locais);
        listaLocais=(Spinner)findViewById(R.id.spinnerLocal);
        listaLocais.setAdapter(adicionaAdapter2);

        botao=(Button)findViewById(R.id.btnEnviar2);
        botao.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        String texto1=parametros.getString("nome");
        String texto2=parametros.getString("sexo");
        String texto3=parametros.getString("estadoCivil");
        String texto4=parametros.getString("telefone");
        String texto5=parametros.getString("tipoTelefone");

        String curso=cursos[listaCursos.getSelectedItemPosition()];
        String local=locais[listaLocais.getSelectedItemPosition()];

        CheckBox manha=(CheckBox)findViewById(R.id.checkManha);
        CheckBox tarde=(CheckBox)findViewById(R.id.checkTarde);
        CheckBox noite=(CheckBox)findViewById(R.id.checkNoite);


        if(!(manha.isChecked())&&!(tarde.isChecked())&&!(noite.isChecked())){
            Toast.makeText(this,"Marque pelo menos uma das opções de horário",Toast.LENGTH_SHORT).show();
        }else{
            intencao=new Intent(this,Tela3.class);

            parametros.putString("nome",texto1);
            parametros.putString("sexo",texto2);
            parametros.putString("estadoCivil",texto3);
            parametros.putString("telefone",texto4);
            parametros.putString("tipoDoTelefone",texto5);
            parametros.putString("curso",curso);

            parametros.putBoolean("manha",manha.isChecked());
            parametros.putBoolean("tarde",tarde.isChecked());
            parametros.putBoolean("noite",noite.isChecked());

            parametros.putString("local",local);
            intencao.putExtras(parametros);
            startActivity(intencao);
        }


    }
}
