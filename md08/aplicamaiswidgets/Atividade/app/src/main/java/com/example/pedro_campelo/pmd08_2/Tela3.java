package com.example.pedro_campelo.pmd08_2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by User on 06/08/2016.
 */
public class Tela3 extends Activity implements View.OnClickListener {
    Intent intencao;
    Bundle parametros;
    Button botao;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela3);

        intencao=getIntent();
        parametros=intencao.getExtras();

        //Este componente captura o valor inserido no campo nome
        TextView nome=(TextView)findViewById(R.id.textNome3);
        nome.setText("Nome:"+parametros.getString("nome"));

        //Este componente captura o valor da opção sexo
        TextView sexo=(TextView)findViewById(R.id.textSexo3);
        sexo.setText("Sexo:"+parametros.getString("sexo"));

        //Este componente captura o valor de uma das opções de estado civil
        TextView estadoCivil=(TextView)findViewById(R.id.textEstadoCivil3);
        estadoCivil.setText("Estado Civil:"+parametros.getString("estadoCivil"));

        //Este componente captura o valor inserido no campo telefone
        TextView telefone= (TextView) findViewById(R.id.textFone3);
        telefone.setText("Telefone:"+parametros.getString("telefone"));

        //Este componente captura o valor da opção tipo de telefone
        TextView tipoTelefone=(TextView)findViewById(R.id.textTipoTelefone3);
        tipoTelefone.setText("Tipo do Telefone:"+parametros.getString("tipoDoTelefone"));

        //Este componente captura o valor da opções de curso
        TextView curso=(TextView)findViewById(R.id.textCurso);
        curso.setText("Curso:"+parametros.getString("curso"));

        //Este componente captura o valor da opções de horário
        TextView horario=(TextView)findViewById(R.id.textHorario);

        if(parametros.getBoolean("manha")){
            horario.setText("Horário: Matutino");
        }else if(parametros.getBoolean("tarde")){
            horario.setText("Horário: Intermediário");
        }else if(parametros.getBoolean("noite")){
            horario.setText("Horário: Noturno");
        }

        if( (parametros.getBoolean("manha")) && (parametros.getBoolean("tarde")) && (parametros.getBoolean("noite")) ){
            horario.setText("Este curso está disponível em todos os horários!!!!!");
        }

        EditText discar=(EditText)findViewById(R.id.editFone3);
        discar.setText(parametros.getString("telefone"));

        botao=(Button)findViewById(R.id.btnDiscar);
        botao.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        //A partir das dos próximos conhecimentos,esta parte do código
        //ficará encarregada de capturar o telefone informado na tela 1
        //e levado a tela 3 para que possa ser feita uma ligação
    }
}
