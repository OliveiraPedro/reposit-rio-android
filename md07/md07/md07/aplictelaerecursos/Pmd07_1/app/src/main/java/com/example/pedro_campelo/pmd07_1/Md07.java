package com.example.pedro_campelo.pmd07_1;

import java.util.ArrayList;

import adapter.MainAdapter;

import objetos.Item;

import com.example.pedro_campelo.pmd07_1.R;

import android.app.Activity;

import android.app.AlertDialog;

import android.content.DialogInterface;

import android.graphics.Bitmap;

import android.graphics.BitmapFactory;

import android.os.Bundle;

import android.view.View;

import android.widget.AdapterView;

import android.widget.ArrayAdapter;

import android.widget.GridView;

import android.widget.TextView;

import android.widget.Toast;

public class Md07 extends Activity {

    GridView gridView;
    ArrayList<Item> gridArray = new ArrayList<Item>();
    MainAdapter mainAdapter;
    TextView tv;
    private AlertDialog alerta;

    @Override

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_md07);
        tv = (TextView) findViewById(R.id.txtStatus);


        Bitmap addUser = BitmapFactory.decodeResource(this.getResources(), R.drawable.novo);
        Bitmap listUser = BitmapFactory.decodeResource(this.getResources(), R.drawable.lista);
        Bitmap search = BitmapFactory.decodeResource(this.getResources(), R.drawable.busca);
        Bitmap close = BitmapFactory.decodeResource(this.getResources(), R.drawable.fechar);

        gridArray.add(new Item(addUser, "Adicionar contato"));
        gridArray.add(new Item(listUser, "Lista de contatos"));
        gridArray.add(new Item(search, "Buscar um contato"));
        gridArray.add(new Item(close, "Fechar"));
        gridView = (GridView) findViewById(R.id.gridview);

        registerForContextMenu(gridView);


        mainAdapter = new MainAdapter(this, R.layout.grid, gridArray);
        gridView.setAdapter(mainAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               selectItem(position+1);
            }
        });
    }


    protected void selectItem(int position) {
        tv.setText("");
        switch (position) {
            case 1:
                adicionarContato();
                break;
            case 2:
                listaDeContatos();
                break;
            case 3:
                buscarContato();
                break;
            case 4:
                fechar();
                break;
            default:
                Toast.makeText(this, "Link não encontrado!",
                        Toast.LENGTH_LONG).show();
                break;
        }
    }

    private void adicionarContato() {
        tv.setText("Adicionar contato selecionado");

        //Lista que recebe os itens do submenu
        ArrayList<String> itens = new ArrayList<String>();
        itens.add("Agenda");
        itens.add("Detalhes do contato");

        //É criado um adapter que recebe um layout especifico para o submenu,álem do contexto e do array criado
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.adicionar_contato, itens);

        //Instancia da class AlertDialog que é utilziada para dar um titulo e icone da a janela do submenu
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Adicionar contato");
        builder.setIcon(R.mipmap.ic_launcher);

        //Define o inicio de um evento ao clicar em uma das opções do submenu
        builder.setSingleChoiceItems(adapter,0,new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialog, int which) {
                alerta.dismiss();
            }
        });

        alerta=builder.create();
        alerta.show();
    }


    private void listaDeContatos() {
        tv.setText("Lista de contatos");
    }

    private void buscarContato(){

        //Cria o gerador do AlertDialog
        AlertDialog.Builder builder=new AlertDialog.Builder(this);

        //Titulo do alerta
        builder.setTitle("Buscar Contato");
        builder.setIcon(R.mipmap.ic_launcher);

        //Mensagem do alerta
        builder.setMessage("Buscar Contato selecionado");

        //Define um botão para o alerta
        builder.setNegativeButton("OK",new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        alerta=builder.create();

        //Exibe o alerta criado anteriormente
        alerta.show();
    }

    private void fechar(){
        //Cria o gerador do AlertDialog
        AlertDialog.Builder builder=new AlertDialog.Builder(this);

        //Titulo do alerta
        builder.setTitle("Fechar Atividade");
        builder.setIcon(R.mipmap.ic_launcher);

        //Definie a mensagem a ser exibida no alerta
        builder.setMessage("Deseja fechar esta atividade?");

        builder.setNegativeButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        builder.setPositiveButton("Não",new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });

        alerta=builder.create();
        alerta.show();
    }
}



