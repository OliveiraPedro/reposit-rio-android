package objetos;

import android.graphics.Bitmap;

/**
 * Created by pedro_campelo on 21/07/2016.
 */
public class Item {

    Bitmap imagem;
    String titulo;

    public Item(Bitmap imagem, String titulo) {
        super();
        this.imagem=imagem;
        this.titulo=titulo;
    }


    public Bitmap getImagem() {
        return imagem;
    }

    public void setImagem(Bitmap imagem) {
        this.imagem = imagem;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }



}

