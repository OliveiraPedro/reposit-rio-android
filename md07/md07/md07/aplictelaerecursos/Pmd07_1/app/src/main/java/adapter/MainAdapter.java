package adapter;

import java.util.ArrayList;
import com.example.pedro_campelo.pmd07_1.R;
import objetos.Item;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MainAdapter extends ArrayAdapter<Item> {
    Context context;
    int layoutResourceId;
    ArrayList<Item> listaItem = new ArrayList<Item>();
    public MainAdapter(Context context, int layoutResourceId,ArrayList<Item> listaItem) {
        super(context, layoutResourceId, listaItem);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.listaItem = listaItem;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View row = convertView;
        RecordHolder holder = null;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new RecordHolder();
            holder.txtTitulo = (TextView)row.findViewById(R.id.item_text);
            holder.imagemItem = (ImageView)row.findViewById(R.id.item_imagem);
            row.setTag(holder);
        } else {
            holder = (RecordHolder) row.getTag();
        }
        Item item = listaItem.get(position);
        holder.txtTitulo.setText(item.getTitulo());
        holder.imagemItem.setImageBitmap(item.getImagem());
        return row;
    }
    static class RecordHolder {
        TextView txtTitulo;
        ImageView imagemItem;
    }
}
