package com.example.pedro_campelo.calculadora;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pedro_campelo.atividade.R;
import com.example.pedro_campelo.atividade.Receiver;

/**
 * Created by pedro_campelo on 30/08/2016.
 */
public class Operacoes extends Activity {

    private Intent intencao;
    private Bundle parametros;
    TextView texto;
    Receiver receiver;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.tela_final);

        IntentFilter filtro = new IntentFilter();
        filtro.addAction("teste");
        receiver=new Receiver();

        registerReceiver(receiver, filtro);

    }

    protected void onDestroy() {
        super.onDestroy();
        //Cancela o registro do receiver
        this.unregisterReceiver(receiver);
    }
}
