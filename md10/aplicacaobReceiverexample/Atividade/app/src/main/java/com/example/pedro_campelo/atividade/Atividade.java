package com.example.pedro_campelo.atividade;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class Atividade extends Activity implements View.OnClickListener{

    private Receiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_atividade);


        IntentFilter filtro=new IntentFilter();
        filtro.addAction("receiver");
        receiver=new Receiver();

        this.registerReceiver(receiver, filtro);

        Button botao=(Button)findViewById(R.id.btnEnviar);

        botao.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        sendBroadcast(new Intent("receiver"));


    }
}
