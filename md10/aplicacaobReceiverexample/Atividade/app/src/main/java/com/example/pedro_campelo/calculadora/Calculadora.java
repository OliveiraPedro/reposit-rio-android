package com.example.pedro_campelo.calculadora;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pedro_campelo.atividade.R;

/**
 * Created by pedro_campelo on 30/08/2016.
 */
public class Calculadora extends Activity implements View.OnClickListener{

    private EditText num1,num2;
    private Button botao;
    Intent intencao;
    Bundle parametros;

    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.operacoes);

        /*IntentFilter filtro=new IntentFilter();
        filtro.addAction("SOMA_VALORES");
        filtro.addCategory("android.intent.category.DEFAULT");*/


        num1=(EditText)findViewById(R.id.editNumero1);
        num2=(EditText)findViewById(R.id.editNumero2);



        //registerReceiver(mReceiver, filtro);

        botao=(Button)findViewById(R.id.btnEnviar);
        botao.setOnClickListener(this);
    }

    /*private final BroadcastReceiver mReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

        }
    };*/

    @Override
    public void onClick(View v) {

        String n1= num1.getText().toString();
        int num1=Integer.parseInt(n1);

        String n2= num2.getText().toString();
        int num2=Integer.parseInt(n2);



    //Recuperação da posicao do item na lista para verificação de qual operação matemática foi escolhida
        intencao=getIntent();
        parametros=intencao.getExtras();
        int operacao=parametros.getInt("operacao");
    //--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//


        intencao=new Intent(this,Operacoes.class);

        parametros.putInt("num1",num1);
        parametros.putInt("num2",num2);
        parametros.putInt("op",operacao);

        intencao.putExtras(parametros);
        startActivity(intencao);
    }
}
