package com.example.pedro_campelo.atividade;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by pedro_campelo on 31/08/2016.
 */
public class Receiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {


        //Feita em segundo plano a execução do método executaAcao(), que gera um texto na janela do LogCat
        executarAcao();
    }

    //Método que gera um texto na janela do LogCat,tendo como parametros, uma string de busca
    // e o texto que irá ser mostrado
    private void executarAcao(){
        Log.d("Receiver","Teste");
    }
}
