package com.example.pedro_campelo.pmd10_1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class Receiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        //Feita em segundo plano a execução do método executaAcao(), que gera um texto na janela do LogCat
        executarAcao();
    }

    //Método que gera um texto na janela do LogCat,tendo como parametros, uma string de busca
    // e o texto que irá ser mostrado
    private void executarAcao(){
        Log.d("Receiver","Teste");
    }
}
