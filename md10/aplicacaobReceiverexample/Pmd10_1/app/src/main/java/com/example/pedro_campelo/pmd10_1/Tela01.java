package com.example.pedro_campelo.pmd10_1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by pedro_campelo on 29/08/2016.
 */
public class Tela01 extends Activity implements View.OnClickListener {

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pmd10_1);

        Button botao=(Button)findViewById(R.id.button);
        botao.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        //Feita a chamada da ação referente a um broadcastReceiver declarado no AndroidManifest.xml
        //Desta maneira, ao clicar no botão, a intent(receiver) é chamada para executar um texto no LogCat
        sendBroadcast(new Intent("teste1"));
    }
}
