package com.example.pedro_campelo.pmd09_1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Pmd09_1 extends Activity implements View.OnClickListener {
    TextView texto;
    Button botao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pmd09_1);


        //O componente button passar a funcionar como um botão ao utilizar a referencia criada na view(arquivo de layout) criado para esta aplicação
        botao = (Button) findViewById(R.id.botao);
        //O componente texto passar a funcionar como um texto ao utilizar a referencia criada na view(arquivo de layout) criado para esta aplicação
        texto = (TextView) findViewById(R.id.idTexto1);
        //A partir deste momento, o componente texto criado anteriormente recebe um texto estático
        texto.setText("Iniciando um Serviço");

        //Tirar dúvida depois com o Silvío
        botao.setOnClickListener((android.view.View.OnClickListener) this);

    }

    /*public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main,menu);
        return true;
    }*/

    @Override
    public void onClick(View v) {

        /*A partir de versões novas do Android(5.0 ou 6.0 - a verificar) a maneira de se chamar um serviço foram modificadas
        * E'necessário que seja criada uma instancia da classe Intent, e em seu construtor passados o contexto , além da classe que você utiliza como serviço.
        * Lembrando que ainda é necessário declarar determinada classe como serviço no AndroidManifest.xml*/

        texto.setText("Iniciando o Serviço...");
        Intent serviceIntent = new Intent(this,MeuServico.class);
        this.startService(serviceIntent);
        Toast.makeText(this,"teste",Toast.LENGTH_SHORT).show();
    }

}
