package com.example.pedro_campelo.pmd09_1;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by pedro_campelo on 08/08/2016.
 */
public class MeuServico extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent,int flags,int startId){
        incrementar();
        stopSelf();
        Log.i("MeusServiços","Encerrando");
        return 0;
    }

    private void incrementar(){
        Log.i("MeusServicos","Iniciando");
        for(int i=0; i<=5; i++){
            try{
                Thread.sleep(500);
                Log.i("MeusServicos", "Valor:" + i);
            }catch(InterruptedException e){
                Log.i("MeusServicos","Erro:"+e);
            }

        }
    }
}
