package com.example.pedro_campelo.pmd09_1;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by pedro_campelo on 10/08/2016.
 */
public class MeuServico2 extends Service {
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate(){
        Log.i("MeusServicos","Executando o método onCreate()");
    }

    public int onStartCommand(Intent intent,int flags,int startId){
        Log.i("MeusServicos","Executando o método onStartCommand()");
        stopSelf();
        return 0;
    }

    public void onDestroy(){
        Log.i("MeusServicos", "Executando o método onDestroy()");
    }
}
