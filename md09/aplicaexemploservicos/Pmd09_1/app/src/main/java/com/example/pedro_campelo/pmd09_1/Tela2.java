package com.example.pedro_campelo.pmd09_1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by pedro_campelo on 10/08/2016.
 */
public class Tela2 extends Activity implements View.OnClickListener{

    TextView texto;
    Button botao;
    Intent intent;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela2);

        botao=(Button)findViewById(R.id.botao);
        texto=(TextView)findViewById(R.id.texto1);

        botao.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        texto.setText("Iniciando o Serviço 2...");
        intent=new Intent(this,MeuServico2.class);
        this.startService(intent);
    }
}
