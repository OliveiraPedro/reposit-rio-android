package com.example.pedro_campelo.atividade;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

/**
 * Created by pedro_campelo on 10/08/2016.
 */
public class ClasseServico extends Service{



    public class ServicoVinculado extends Binder {
        ClasseServico getService(){
            return ClasseServico.this;
        }
    }



    @Override
    public IBinder onBind(Intent intent) {
        IBinder binder=new ServicoVinculado();
        return binder;
    }

    public int operacoes(String texto, int num1, int num2){

        int resultado = 0;

        if(texto.equals("Soma")){
            resultado= num1+num2;
        }else if(texto.equals("Subtração")){
            resultado= num1-num2;
        }else if(texto.equals("Multiplicação")){
            resultado= num1*num2;
        }else if(texto.equals("Divisão")){
            resultado= num1/num2;
        }
        return  resultado;
    }


}
