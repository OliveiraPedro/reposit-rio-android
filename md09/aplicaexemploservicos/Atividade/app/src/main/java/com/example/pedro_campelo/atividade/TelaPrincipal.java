package com.example.pedro_campelo.atividade;

import com.example.pedro_campelo.atividade.ClasseServico.ServicoVinculado;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class TelaPrincipal extends Activity implements View.OnClickListener{
    ClasseServico mServico;
    boolean mVinculo=false;
    Button botao;
    Spinner listaOP;
    Intent intent;

    static String[] listaOperacoes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_principal);


        listaOperacoes=getResources().getStringArray(R.array.operacoes);
        ArrayAdapter<String> addLista=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,listaOperacoes);
        listaOP=(Spinner)findViewById(R.id.spinnerOP);
        listaOP.setAdapter(addLista);

        botao=(Button)findViewById(R.id.btnEnviar);
        botao.setOnClickListener(this);

    }

    protected void onStart(){
        super.onStart();

        //Criando a instancia da classe intent, passando como parametros o
        // contexto da activity(this) mais a classe de serviço utilizada
        intent=new Intent(this,ClasseServico.class);

        //Utilização do método bindService() sendo passado como parametros
        // a intent(configurada com as informações do servico no .xml), implementação da classe ServiceConnection mServicoConexao
        // para gerenciar as informações do serviço e a constante BIND_AUTO_CREATE para criar o vínculo.
        bindService(intent,mServicoConexao, Context.BIND_AUTO_CREATE);
    }

    //Método utilizado para determinar o momento em que o serviço para de funcionar,
    //utilizando unbindService, recebendo as configurações da implementação ServiceConnection mServicoConexao
    protected void onStop(){
        super.onStop();
        if(mVinculo){
            unbindService(mServicoConexao);
            mVinculo=false;
        }
    }

    @Override
    public void onClick(View v) {

        //Recupera a posição do elemento selecionado dentro do Spinner
        //Recupera o texto do elemento selecionado dentro do Spinner, sendo que utiliza a posição recuperada anteriormente
        String operacoes=listaOperacoes[listaOP.getSelectedItemPosition()];

        //Recupera o texto digitado no campo nome
        EditText num1=(EditText)findViewById(R.id.editNumero1);
        int numero1= Integer.parseInt(num1.getText().toString());

        //Recupera o texto digitado no campo nome
        EditText num2=(EditText)findViewById(R.id.editNumero2);
        int numero2= Integer.parseInt(num2.getText().toString());
       // Toast.makeText(this,"O resultado final foi:teste",Toast.LENGTH_SHORT).show();

        if(mVinculo){
            int resultadoFinal = 0;
            try{
                resultadoFinal=mServico.operacoes(operacoes,numero1,numero2);
                Toast.makeText(this,"O resultado final foi:"+resultadoFinal,Toast.LENGTH_SHORT).show();
            }catch(ArithmeticException e){
                messageBox("Não é possivel efetuar divisão por zero",e.getMessage());
            }

        }

    }

    private ServiceConnection mServicoConexao=new ServiceConnection() {
        @Override

        //Depois de uma conexão estabelecida, a classe ServiceConnection executa o método
        //onServiceConected() que utiliza uma instancia da classe interna ServicoVinculado para recuperar
        //a instancia do serviço com o getService()
        public void onServiceConnected(ComponentName name, IBinder service) {

            ServicoVinculado servicoVinculado=(ServicoVinculado) service;
            mServico=servicoVinculado.getService();
            mVinculo=true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mVinculo=false;
        }
    };

    //Método sem retorno utilizado para criar uma mensagem caso o usuário tendo dividir por zero
    private void messageBox(String texto, String memensagem){
        Log.d("EXCEPTION: " + texto,  memensagem);

        Toast.makeText(this,texto,Toast.LENGTH_SHORT).show();
    }

}

