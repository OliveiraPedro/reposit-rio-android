package com.example.pedro_campelo.pmd09_2;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.example.pedro_campelo.pmd09_2.MeuServico3.ServicoVinculado;

public class Tela3 extends Activity implements View.OnClickListener{
    MeuServico3 mServico;
    boolean mVinculo=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela3);

        Button botao=(Button)findViewById(R.id.mOKButton);
        botao.setOnClickListener(this);
    }

    protected void onStart(){
        super.onStart();
        //Criando a instancia da classe intent, passando como parametros o
        // contexto da activity(this) mais a classe de serviço utilizada
        Intent intent=new Intent(this,MeuServico3.class);

        //Utilização do método bindService() sendo passado como parametros
        // a intent(configurada com as informações do servico no .xml), implementação da classe ServiceConnection mServicoConexao
        // para gerenciar as informações do serviço e a constante BIND_AUTO_CREATE para criar o vínculo.
        bindService(intent,mServicoConexao, Context.BIND_AUTO_CREATE);
    }

    //Método utilizado para determinar o momento em que o serviço para de funcionar,
    //utilizando unbindService, recebendo as configurações da implementação ServiceConnection mServicoConexao
    protected void onStop(){
        super.onStop();
        if(mVinculo){
            unbindService(mServicoConexao);
            mVinculo=false;
        }
    }

    @Override
    public void onClick(View v) {
        if(mVinculo){
            //O método obterNumeroRamdomico é utilizado para pegar, como o próprio nome diz
            //um numero dentre 100
            int num=mServico.obterNumeroRamdomico();
            Toast.makeText(this,"Número:"+num,Toast.LENGTH_SHORT).show();
        }

    }

    private ServiceConnection mServicoConexao=new ServiceConnection() {
        @Override

        //Depois de uma conexão estabelecida, a classe ServiceConnection executa o método
        //onServiceConected() que utiliza uma instancia da classe interna ServicoVinculado para recuperar
        //a uma instancia do serviço com o getService()
        public void onServiceConnected(ComponentName name, IBinder service) {

            ServicoVinculado servicoVinculado=(ServicoVinculado) service;
            mServico=servicoVinculado.getService();
            mVinculo=true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mVinculo=false;
        }
    };
}
