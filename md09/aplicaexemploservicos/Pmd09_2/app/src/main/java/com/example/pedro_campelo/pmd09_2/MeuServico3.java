package com.example.pedro_campelo.pmd09_2;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.Random;

/**
 * Created by pedro_campelo on 10/08/2016.
 */
public class MeuServico3 extends Service{

    public class ServicoVinculado extends Binder{
        MeuServico3 getService(){
            return MeuServico3.this;
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        IBinder mBinder=new ServicoVinculado();
        return mBinder;
    }

    public int obterNumeroRamdomico(){
        Random mGerador=new Random();
        return mGerador.nextInt(100);
    }
}
